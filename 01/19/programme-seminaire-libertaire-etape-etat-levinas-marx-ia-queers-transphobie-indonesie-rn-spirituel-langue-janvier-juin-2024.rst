.. index::
   pair: ETAPE ; Programme séminaire libertaire Etape : Etat, Levinas-Marx, IA, queers-transphobie, Indonésie, RN, spirituel, langue (janvier-juin 2024)
   pair: Philippe Corcuff ; Programme séminaire libertaire Etape : Etat, Levinas-Marx, IA, queers-transphobie, Indonésie, RN, spirituel, langue (janvier-juin 2024)

.. _etape_2024_01_19:

=================================================================================================================================================================================
2024-01-19 **Programme séminaire libertaire Etape : Etat, Levinas-Marx, IA, queers-transphobie, Indonésie, RN, spirituel, langue (janvier-juin 2024)** par Philippe Corcuff
=================================================================================================================================================================================

- http://www.grand-angle-libertaire.net/

Auteur Philippe Corcuff
=========================

- :ref:`antisem:philippe_corcuff`


Présentation
===============

Recevez ci-après pour information le programme du séminaire de recherche
militante et libertaire ETAPE (Explorations Théoriques Anarchistes Pragmatistes
pour l'Emancipation) entre janvier et juin 2024.

Les deux rencontres publiques dans la librairie Publico (Paris 11e), les
jeudis 25 janvier et 27 juin 2024, sont ouvertes à toutes et tous.

Les séances de séminaire des vendredis 26 janvier, 1er mars, 29 mars,
26 avril, 24 mai et 28 juin 2024 ne sont pas publiques.

Pour pouvoir connaître le lieu de ces séances (à Paris), il faut le
demander à philippe.corcuff@sciencespo-lyon.fr.

bon WE Philippe Corcuff


Programme du séminaire de recherche militante et libertaire Janvier-juin 2024
=======================================================================================

ETAPE (Explorations Théoriques Anarchistes Pragmatistes pour l'Emancipation)

Co-animé par Philippe Corcuff, Didier Eckel et Guy Lagrange

Textes publiés sur le site de réflexions libertaires Grand Angle :
http://www.grand-angle-libertaire.net/

Les séances ont lieu une fois par mois un vendredi, entre 19h et 22h, à Paris.

Chacun amène à boire et à grignoter (autour d’une dizaine de participants)
ce qui sera fait, en toute convivialité, pendant le séminaire !


Jeudi 25 janvier 2024 - Rencontre publique, librairie Publico (145 rue Amelot Paris 11e), 19h-21h30
=====================================================================================================

- http://www.atelierdecreationlibertaire.com/Repenser-l-Etat-au-XXIe-siecle.html

Débat autour du dernier livre du séminaire ETAPE : Repenser l'État au XXIe siècle.

Libertaires et pensées critiques (Atelier de création libertaire, novembre 2023,
http://www.atelierdecreationlibertaire.com/Repenser-l-Etat-au-XXIe-siecle.html)

Table-ronde, animé par le militant anarchiste **Guy Lagrange**, composée de trois
des co-auteurs-trices du livre :

- le sociologue **Albert Ogien**,
- la sociologue **Sylvaine Bulle**
- et le politiste **Philippe Corcuff** ;

la philosophe (et animatrice de l'émission "Avec philosophie" sur France Culture)
**Géraldine Muhlmann** y étant la discutante

Rencontre ouverte à toutes et à tous


Vendredi 26 janvier 2024 Débat autour du livre de la philosophe Lucie Doublet : Emmanuel Levinas et l'héritage de Karl Marx
==================================================================================================================================

- https://www.lecteurs.com/livre/emmanuel-levinas-et-lheritage-de-karl-marx-sublime-materialisme/5739699


Débat autour du livre de la philosophe **Lucie Doublet** : Emmanuel Levinas
et l'héritage de Karl Marx. Sublime matérialisme
(édition Orante, 2021, https://www.lecteurs.com/livre/emmanuel-levinas-et-lheritage-de-karl-marx-sublime-materialisme/5739699).

En présence de l’autrice, discussion précédée de deux rapports sur le
livre :

- par **Thomas Chust**, chercheur en astrophysique au CNRS et ancien militant
  anticapitaliste
- et par **Philippe Corcuff**, professeur de science politique et militant libertaire


.. _ia_2024_03_01:

Vendredi 1er mars 2024 **"Sociologie politique de l'intelligence artificielle.  Peut-on parler d'un confusionnisme des Big Tech ?"** avec Bilel Benbouzid
==========================================================================================================================================================

- https://www.radiofrance.fr/personnes/bilel-benbouzid
- https://www.cairn.info/revue-reseaux-2022-2-page-9.htm?contenu=article
- https://www.cairn.info/revue-reseaux-2022-2-page-29.htm?tap=ljhyxge8glg5r&wt.mc_id=crn-tap-a-747823&contenu=article

"Sociologie politique de l'intelligence artificielle.
Peut-on parler d'un confusionnisme des Big Tech ?"

Intervention de **Bilel Benbouzid** qui, après une formation d’ingénieur des
travaux publics de l’État, est devenu maître de conférences en sociologie
à l’Université Gustave-Eiffel.

Il est spécialiste de la sociologie de l’action publique et de la sociologie
digitale.
Il a notamment publié dans le n° 232-233 de la revue Réseaux de 2022 :
avec Dominique Cardon,

- `"Contrôler les IA" <https://www.cairn.info/revue-reseaux-2022-2-page-9.htm?contenu=article>`_
- et, avec  Yannick Meneceur et Nathalie Alisa Smuha, `"Quatre nuances de régulation de l’intelligence
  artificielle. Une cartographie des conflits de définition" <https://www.cairn.info/revue-reseaux-2022-2-page-29.htm?tap=ljhyxge8glg5r&wt.mc_id=crn-tap-a-747823&contenu=article>`_.

Il est intervenu à plusieurs reprises sur Radio France à propos de ces
questions : https://www.radiofrance.fr/personnes/bilel-benbouzid.


Vendredi 29 mars 2024 "Approches anarcho-queers et critique de la transphobie"
==================================================================================

- https://www.cairn.info/revue-du-mauss1-2023-2-page-147.htm?contenu=article

"Approches anarcho-queers et critique de la transphobie"

Intervention d’**Erwan Sommerer**, maître de conférences en science politique
à l’Université d’Angers et membre du collectif de rédaction de la
revue **Réfractions. Recherches et expressions anarchistes**.

Il travaille sur l’histoire de la pensée politique en période révolutionnaire,
ainsi que sur le lien entre conflictualité politique, fluidité des identités
et anarchisme.

Il a publié récemment : `"Résistance à la réification et désidentification :
du constructivisme à l’anarchisme" <https://www.cairn.info/revue-du-mauss1-2023-2-page-147.htm?contenu=article>`_, Revue du MAUSS, n° 62, 2023.



Vendredi 26 avril 2024 "De l'anthropologie à la création théâtrale : recherche, luttes politiques et expériences dissensuelles en Indonésie (2016-2019)"
=================================================================================================================================================================

- https://www.cairn.info/revue-multitudes-2017-2-page-57.htm
- https://www.cairn.info/revue-chimeres-2017-2-page-23.htm

"De l'anthropologie à la création théâtrale : recherche, luttes politiques
et expériences dissensuelles en Indonésie (2016-2019)"

Intervention de **Kassia Aleksic**, qui a soutenu à l’Université Paris Cité
en novembre 2023 une thèse d’anthropologie politique sur Le terrain, une
scène du réel ? Itinérances politiques, ruptures anthropologiques (Indonésie, 2016-2019).

Elle a notamment publié en 2017:

- `"La théâtralisation d’une lutte «écoféministe»", revue Multitudes <https://www.cairn.info/revue-multitudes-2017-2-page-57.htm>`_, n° 67,
- `"Portrait d’une ouvrière devenue lesbienne et dirigeante syndicale" <https://www.cairn.info/revue-chimeres-2017-2-page-23.htm>`_, revue Chimères, n° 92.



Vendredi 24 mai 2024, 18h-20h (attention plus tôt que d’habitude) "Avant les élections européennes : la science politique et le Rassemblement national"
===============================================================================================================================================================

- https://www.institutmontaigne.org/en/expressions/undermining-democracy-marine-le-pen-and-eric-zemmours-populist-politics

"Avant les élections européennes : la science politique et le Rassemblement
national"

Intervention de **Nonna Mayer**, directrice de recherche émérite en science
politique au CNRS ; autrice, parmi de multiples écrits, de :

- Ces Français qui votent Le Pen (Flammarion, 2002) ; avec Sylvain Crépon
  et Alexandre Dézé,
- Les faux-semblant du Front national. Sociologie d’un parti politique (Presses
  de Sciences Po, 2015) ;
- et de `"Undermining Democracy: Marine Le Pen and Eric Zemmour’s Populist Policy" <https://www.institutmontaigne.org/en/expressions/undermining-democracy-marine-le-pen-and-eric-zemmours-populist-politics>`_, Institut Montaigne,
  14 avril 2022.



Jeudi 27 juin 2024 - Rencontre publique, librairie Publico (145 rue Amelot - Paris 11e), 19h-21h30 "Le spirituel : piège ou souffle d'une politique émancipatrice ?"
=========================================================================================================================================================================

- https://www.lesbelleslettres.com/livre/9782251453576/l-imposture-du-theologico-politique

"Le spirituel : piège ou souffle d'une politique émancipatrice ?"

Débat autour des livres de la philosophe **Géraldine Muhlmann**,
L’imposture du théologico-politique (Les Belles Lettres, 2022,
https://www.lesbelleslettres.com/livre/9782251453576/l-imposture-du-theologico-politique)
et du théologien catholique **Jérôme Alexandre**, Le christianisme est un
anarchisme (à paraître en mai 2024 aux éditions Textuel), en présence
de l’autrice et de l’auteur, animé par Stéphane Lavignotte (pasteur,
théologien et militant écolo-libertaire)

Rencontre ouverte à toutes et à tous


Vendredi 28 juin 2024 "En quoi «la langue» n'est pas fasciste : s'émanciper par un imaginaire hétérolingue"
=================================================================================================================

- https://www.enfrancaisaupluriel.fr/
- https://www.theatre-contemporain.net/video/La-langue-encore-En-corps-conference-de-Myriam-Suchet-Les-Zebrures-du-printemps-2023

"En quoi «la langue» n'est pas fasciste : s'émanciper par un imaginaire
hétérolingue"

Intervention de **Myriam Suchet**, maitresse de conférences en langues et
littératures françaises et francophones à l’Université Sorbonne
Nouvelle. Elle enseigne, cherche et se perd beaucoup, que ce soit à la
Sorbonne nouvelle, où elle dirige le Centre d'études québécoises, qu'en
dehors de l’institution universitaire, dans les brèches et les interstices
collectifs. Sa recherche-action-création, résolument indisciplinée,
repose sur l'évidence que "la langue", ça n'existe pas.

Voir son site : `En français au pluriel <https://www.enfrancaisaupluriel.fr/>`
et la captation vidéo de sa conférence lors du Festival des écritures de
Limoges "Les zébrures du printemps", le 23 mars 2023 : `"«La langue» encore ? En corps !" <https://www.theatre-contemporain.net/video/La-langue-encore-En-corps-conference-de-Myriam-Suchet-Les-Zebrures-du-printemps-2023>`_ [environ 46 mn]

Repères de la communication
------------------------------

Lorsqu'on accueille des personnes exilées, traduire le français langue
étrangère en français langue étrangée, c'est offrir l’hospitalité à toutes
les différences : celles qui existent entre les langues et aussi celles
qui constituent chacune de l’intérieur.

Tolérer la diversité ne suffit pas : lisons résolument le "s" de «français»
comme une marque de pluriel !

Et tirons les conséquences qui en découlent pour une recherche-action-création.
