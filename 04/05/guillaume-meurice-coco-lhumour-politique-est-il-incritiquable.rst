.. index::
   pair: Guillaume Meurice/Coco : l’humour politique est-il incritiquable ? ; Philippe Corcuff (2024-04-05)

.. _corcuff_2024_04_05:

============================================================================================================
2024-04-05 **Guillaume Meurice/Coco : l’humour politique est-il incritiquable ?** par Philippe Corcuff
============================================================================================================

- https://blogs.mediapart.fr/philippe-corcuff/blog/050424/guillaume-meuricecoco-l-humour-politique-est-il-incritiquable


Préambule
================

En revenant sur une "blague " controversée de Guillaume Meurice, via son
récent livre "Dans l’oreille du cyclone", et sur un dessin de Coco passé
inaperçu en décembre, une interrogation sur les dérapages politiques de
l’humour à propos des drames du Proche-Orient, aérée par l'ouvrage de
Michel Wieviorka : "La dernière histoire juive"…

Introduction
================

**Il est attristant** que des humoristes (comme Guillaume Meurice) et des
dessinatrices (comme Coco) reçoivent des courriers d’insultes (moi-même,
bien qu’ayant bien peu d’humour, j’en reçois de temps en temps pour
ce que j’ai pu écrire ou dire ici ou là).

**Il est inacceptable** qu’ils soient l’objet de menaces de mort ; et c’est
encore plus ignoble dans le cas de Coco, en tant que rescapée des assassinats
djihadistes de mes amis de Charlie Hebdo le 7 janvier 2015 (:ref:`1 <1_corcuff_2024_04_05>`).

Intenter une action judiciaire contre Meurice, alors qu’il n’y avait pas
d’incitation à la haine raciale dans sa "blague" sur France Inter du 29 octobre
2023 – ce qui ne veut pas dire qu’elle ne portait pas des ambiguïtés du point de vue
de l’antisémitisme, particulièrement risquées dans un contexte de remontée
de l’antisémitisme en France – **est une mauvaise chose pour la liberté d’expression**.

**Et pourtant des traits d’humour de Meurice et des dessins de Coco, et d’autres,
peuvent être critiqués**.

Car il n’y a aucune raison qu’une modalité de la critique – sa forme humoristique –
**puisse prétendre tout critiquer sans pouvoir être critiquée à son tour**.

**Dans le cas contraire, ce serait également une mauvaise chose pour la liberté
d’expression dans des sociétés pluralistes à idéaux démocratiques**.

Guillaume Meurice ou comment publier aux éditions du Seuil **un livre de 171 pages dont le presque vide est rempli par l’autojustification (ou l’effet Naulleau-Onfray)**
============================================================================================================================================================================

J’ai apprécié un précédent livre publié par Meurice en 2023 sous le
titre Petit éloge de la médiocrité (:ref:`2 <2_corcuff_2024_04_05>`).

Je l’ai écrit dans une chronique mensuelle que je fais sur le site du Nouvel Obs (:ref:`3 <3_corcuff_2024_04_05>`).
On peut lire dans cet ouvrage de l'humoriste de France Inter une ode à la
place des fragilités ordinaires en politique.
Et Meurice lance à un moment de manière suggestive :"Un peu d’humilité ne nuirait
pas face à ce qui nous dépasse complétement" (p. 95).

Mais, patatras !, sa "blague" douteuse du 29 octobre 2023 dans l‘émission
"Le grand dimanche soir" sur France Inter, avec la caricature du premier
ministre israélien Benjamin Netanyahu en "une sorte de nazi sans prépuce",
a ouvert de nouvelles interrogations.

Antisémite Meurice ? Non, mais il a mis le doigt, sans s’en rendre compte,
dans une assimilation entre "juif" et "nazi" qui est devenu un stéréotype
antisémite (du type : "Regardez les Juifs font aux Palestiniens ce que les
nazis leur ont fait").

Et cela tombe dans un contexte inflammable où des usages antisémites de
telles ambiguïtés sont davantage probables.

Par ailleurs, il a pu alimenter une relativisation à pente négationniste de
la Shoah dans le rapprochement avec les massacres en cours à Gaza.

Et loin de faire preuve de "l’humilité" dont il se réclamait en 2023, il
s’est enfermé dans la dénégation.
Ce qui m’a conduit à rectifier mon jugement dans la chronique qui a suivi (:ref:`4 <4_corcuff_2024_04_05>`).

Meurice vient même de consacrer tout un livre à l'autojustification de son
29 octobre 2023 : Dans l’oreille du cyclone (:ref:`5 <5_corcuff_2024_04_05>`).
Les prestigieuses éditions du Seuil ont ainsi publié un ouvrage de 171 pages
**largement vide de contenu éthique, politique et intellectuel, très peu drôle,
mais empli d’un bétonnage dans le déni, sans qu’une moindre faille ou faiblesse
puisse être reconnue** :

"je ne comprends pas comment je pourrais m’excuser d’une faute que
je n’ai pas commise" (p. 38),
"n’ayant pas commis de faute…" (p. 66)…

Selon ce narcissisme dogmatique, le Mal ne viendrait que de l’extérieur, du
"Système", mais il ne pénètrerait jamais en soi (chez les autres, oui, souvent
même - et c’est contre les turpitudes des autres que l’humour de Meurice est
principalement dirigé en général – mais moi vous n’y pensez pas, j’ai des
couilles de gauche en béton !).

**On est loin de la vigilance du grand théoricien critique Theodor Adorno
appelant à "aussi penser contre soi-même"** (:ref:`6 <6_corcuff_2024_04_05>`).

Charline Vanhoenacker a eu davantage le sens autocritique des fragilités
humoristiques - cela pourrait être lié à une tendance genrée – quand elle a
demandé à Meurice de "prendre la parole pour tenter d’apaiser les choses" (p. 78).

Elle s’est néanmoins heurtée à un mur viriliste.

Ça ressemble à un récent livre de dialogue entre Éric Naulleau et Michel Onfray,
La gauche réfractaire (:ref:`7 <7_corcuff_2024_04_05>`), où les deux compères, dans leurs cheminements
respectifs de la gauche vers l’extrême droite, apparaissent focalisés sur
tous les gens qui leur voudraient du mal en bétonnant l’autojustification
à partir d’un certain vide intellectuel.

Et Meurice a pourtant le culot de l’autocélébration intellectuelle :
"j’aime le questionnement" (p. 136)… en tout cas en ne s’aventurant pas trop
loin de son ego.
Et il se met alors nu devant nous (pas trop nu, quand même, car il demeure
fort habillé de certitudes) en tant que victime : "Ainsi va la vie d’un humoriste en France, en 2023"
(p. 134)…
Les premières larmes coulent sur nos joues attendries…

Cependant le moi de Meurice prétend s’étendre à une cause politique
générale (c’est le "moi Meurice, Président des humoristes… et phare
de la gauche radicale ").

Il nous assène ainsi la preuve par l’extrême droite de sa totale innocence
avec un arrière-goût complotiste : une "polémique montée de toutes pièces,
instrumentalisée par une extrême droite qui ne rêve que de privatisation du
service public audiovisuel" (p. 21) et "l’agenda politique de l’extrême droite" (p. 24).

Si on est vraiment de gauche, on est immédiatement au garde à vous, de
nouveau la larme à l’œil.
"Tout le monde se lève pour Meurice, Meurice, Meurice… "
Et si on ne fait  pas le jeu de l’extrême droite en osant critiquer ce
qui ne serait pas critiquable, on se vautre soit

- dans "la malhonnêteté intellectuelle" (p. 76)
- et "la mauvaise foi " (p. 139),

soit

- dans "l’hypocrisie" (p. 115),
- soit dans "la lâcheté " (p. 72, p. 163 et p. 168).

Je crains d’être, pour les fans de Meurice dans la gauche radicale
(ma famille politique qui m’adore !) qui vont s’exprimer sur le fil de
commentaires de ce billet de blog de Mediapart, tout à la fois un "allié
objectif de l’extrême droite", d’une "malhonnêteté intellectuelle"
et d’une "mauvaise foi" crasses, un "hypocrite" et un "lâche",
voire un "con", selon l’expression de "l’amour révolutionnaire"
d’Houria Bouteldja à mon égard sur sa page Facebook le 2 avril 2024.

À un moment, il semble que Meurice va s’extirper de ce fatras
d’autojustification politique en ouvrant une piste potentiellement
autocritique : "J’ai d’ailleurs toujours eu du mal à comprendre
comment un humoriste pouvait se prendre au sérieux, son métier consistant
précisément à lutter contre cet esprit et faire dégonfler les têtes"
(p. 103).

Toutefois il ne comprend pas qu’il s’adresse la critique à lui-même.
Et la mécompréhension de son effort d’autoréflexion critique l’empêche de
faire dégonfler sa propre tête. C’est ballot !

Meurice  ne semble pas s’intéresser aux sciences sociales.

Il n’est pas le seul : c’est assez courant aujourd’hui aussi parmi les politiciens
et les technocrates qui nous gouvernent, quand ils n’en font pas un être maléfique,
"islamogauchiste" et "wokiste", qui menacerait La Civilisation (:ref:`8 <8_corcuff_2024_04_05>`).

Or, les sciences sociales mettent souvent en évidence que le sens des mots et
des paroles ne dépend pas seulement des intentions de leurs locuteurs,
mais aussi et surtout du contexte social, idéologique et politique dans
lequel ils apparaissent.

Cela fragilise quelque peu l’ego qui campe sur ses intentions.

Cela aurait pu plaire au Meurice du Petit éloge de la médiocrité, pas à celui
du 29 octobre et de ses suites.
C’est à cause des faiblesses de nos intentions individuelles dans les effets
de nos paroles et de nos actes sur le cours du monde qu’un des fondateur de
la sociologie moderne, Max Weber, a forgé la notion d’éthique de responsabilité (:ref:`9 <9_corcuff_2024_04_05>`),
c’est-à-dire qui ne se soucie pas seulement des principes et des intentions
mais aussi des conséquences, en ce qu’elles échappent justement souvent aux
principes et aux intentions.

Pas de contexte chez Meurice, que de bonnes intentions !
=============================================================

Quand on lui parle des actes antisémites en augmentation, il ne comprend
"pas le lien entre ce que j’ai pu dire et ces derniers. D’ailleurs,
il n’y en a aucun" (p. 64).

L’intention individuelle de la bonne âme humoristique de gauche radicale est
toute-puissante.

C’est pourquoi, pour lui, une blague est "antisémite" ou "ne l’est pas" (p. 76) :
il n’y a pas d’entre-deux, pas de zone grise entre le noir et le blanc.

Comme le notait le philosophe Clément Rosset, "Il n’est pas de remède contre la
clairvoyance : on peut prétendre éclairer celui qui voit trouble, pas celui
qui voit clair" (:ref:`10 <10_corcuff_2024_04_05>`).

Cependant l’antisémitisme ou l’islamophobie, ou le sexisme, ou l’homophobie,
ou la transphobie ne se réduisent pas à des intentions antisémites, islamophobes,
sexistes, homophobes ou transphobes.

Car il peut y avoir des effets antisémites, islamophobes, sexistes, homophobes
ou transphobes sans que cela ne passe par de telles intentions (:ref:`11 <11_corcuff_2024_04_05>`).

Un dessin de Coco comme illustration du narratif du gouvernement israélien

L’humour alimente fréquemment les controverses politiques autour des massacres
perpétrés au Proche-Orient ces derniers temps en France.
La députée LFI Sophia Chikirou a réagi violemment à un dessin de la dessinatrice Coco paru
dans Libération le 11 mars dernier : "Vous n’aurez pas notre haine mais vous la méritez".

Une vague d’insultes et de menaces de mort inadmissibles a, après le tweet
de Chikirou, déferlé sur Coco.

Pour ma part, je ne vois pas de problème dans ce dessin, en ce qu’il montre
l’horreur de la famine à Gaza généré par l’intervention militaire israélienne,
tout en ajoutant une ironie critique vis-à-vis de la religion dominante sur
ce territoire.

Par contre, un autre dessin, antérieur, de Coco, datant de décembre 2023,
a une portée politique beaucoup plus inquiétante. Et pourtant il n’a pas
suscité de polémique. Il faut dire qu’il n’a pas eu la même existence
publique : il fait partie de ses dessins de presse qui ne sont pas publiés,
le dessinateur réalisant souvent plusieurs dessins dont un seul va être
sélectionné. À ma connaissance (qui peut être défaillante), celui-là
n’a paru ni dans Charlie Hebdo, ni dans Libération.

Il a quand même connu une petite existence publique via Charlie Déchaîné
(@CharlieDechaine) qui publie chaque jour une sélection de dessins de presse
et de caricatures sur X (ex-Twitter).
Il a publié ce dessin de Coco le 10 décembre 2023, qui a dû donner son autorisation
pour que cela puisse se faire (:ref:`12 <12_corcuff_2024_04_05>`) :

Ce dessin reprend le narratif du gouvernement israélien selon lequel le
responsable des dizaines de milliers de civils, dont beaucoup d’enfants,
serait le Hamas et pas le gouvernement israélien.

Ce ne serait pas les bombes israéliennes qui tueraient mais le Hamas, en
faisant des enfants des boucliers humains.
Le Hamas est responsable des crimes contre l’humanité du 7 octobre
en Israël, mais les crimes de guerre à Gaza sont de la responsabilité
de l’armée israélienne.
Et si l’armée israélienne arrête ses bombardements, les massacres s’arrêtent.
Il y a quelque chose d’horrible dans le redoublement dessiné ici par Coco
de la propagande gouvernementale israélienne légitimant une tuerie.
Quelque chose qui doit pouvoir être critiqué politiquement, même si ce point
de vue doit bénéficier de la libre expression.

J’avais en juin 2023 opposé l’humour de gauche de Charline Vanhoenacker
et de sa bande (sur France Inter) à l’humour de droite de Gaspard Proust
(sur l’Europe 1 de Vincent Bolloré) et sa litanie de lieux communs
réactionnaires (:ref:`13 <13_corcuff_2024_04_05>`).

Avec la "blague" de Meurice et le dessin de Coco, cela devient plus compliqué.

**Il manque notamment quelque chose à cet humour de gauche pour alimenter,
via l’ironie, un imaginaire émancipateur : le sens de l’autodérision**.

Sur ce plan, **le dernier livre du sociologue Michel Wieviorka peut s’avérer utile**.

Michel Wieviorka et les "histoires juives"
=================================================

Wieviorka a publié en novembre 2023 : La dernière histoire juive. Âge d’or
et déclin de l’humour (Denoël) (:ref:`14 <14_corcuff_2024_04_05>`).

Le sociologue contextualise le genre humoristique nommé "histoires juives" : il
prendrait son essor à la fin des années 1960 aux États-Unis (avec des figures
cinématographiques comme Woody Allen, Mel Brooks ou les frères Joel et Ethan Coen),
tout en puisant dans l’expérience antérieure du shtetl et du yiddish dans
l’Europe de l’Est d’avant la Seconde Guerre mondiale.

Mais cet humour juif rebondit aussi en France avec, par exemple, le film
Les Aventures de Rabbi Jacob de Gérard Oury (1973).

Wieviorka met en évidence que, à la différence, de nombre d’histoires
drôles qui "reposent sur un stéréotype" en disqualifiant "la cible
visée pour mieux valoriser le narrateur et son public", **il y a de
"l’autodérision " dans ces "histoires juives" (p. 39)**, mais également
un **"sens de l’absurde éventuellement poussé très loin"** (p. 90),
dans un rapport à une histoire collective aux composantes tragiques.

**Ce type d’humour ouvre en pratique le questionnement contre les certitudes
dogmatiques, comme dans cette histoire reprise par le sociologue (p. 177) :**

::

    Un goy et un Juif conversent dans un train.

    - Pourquoi vous, les Juifs, lorsqu’on vous pose une question, répondez
    toujours par une autre question ?

    - Pourquoi pas ?

Notes
===========

.. _1_corcuff_2024_04_05:

(1) Philippe Corcuff, "Mon ami Charb : les salauds, les cons, l’émotion ordinaire et la tendresse ", blog Mediapart, 8 janvier 2015
---------------------------------------------------------------------------------------------------------------------------------------

Philippe Corcuff, "Mon ami Charb : les salauds, les cons, l’émotion ordinaire
et la tendresse ", blog Mediapart, 8 janvier 2015.

.. _2_corcuff_2024_04_05:

(2) Guillaume Meurice, Petit éloge de la médiocrité, Éditions Les Pérégrines, 2023
---------------------------------------------------------------------------------------

Guillaume Meurice, Petit éloge de la médiocrité, Éditions Les Pérégrines, 2023.

.. _3_corcuff_2024_04_05:

(3) Philippe Corcuff, "De Israël/Gaza à Arras : pour une politique de la fragilité ", site du Nouvel Obs, 17 octobre 2023
----------------------------------------------------------------------------------------------------------------------------

Philippe Corcuff, "De Israël/Gaza à Arras : pour une politique de la fragilité ", site du Nouvel Obs, 17 octobre 2023.

.. _4_corcuff_2024_04_05:

(4) Philippe Corcuff, "De nos intimités désorientées à une gauche déréglée par le Proche-Orient", site du Nouvel Obs, 22 novembre 2023
----------------------------------------------------------------------------------------------------------------------------------------

Philippe Corcuff, "De nos intimités désorientées à une gauche
déréglée par le Proche-Orient", site du Nouvel Obs, 22 novembre 2023.

.. _5_corcuff_2024_04_05:

(5) Guillaume Meurice, Dans l’oreille du cyclone, Seuil, mars 2024.
--------------------------------------------------------------------------

Guillaume Meurice, Dans l’oreille du cyclone, Seuil, mars 2024.

.. _6_corcuff_2024_04_05:

(6) Theodor Adorno, Dialectique négative (1e éd. : 1966), Payot, 1992, p. 286.
----------------------------------------------------------------------------------

- https://www.placedeslibraires.fr/listeliv.php?base=allbooks&mots_recherche=Theodor+Adorno%2C+Dialectique+n%C3%A9gative
- https://www.librairielesquare.com/livre/9782228897754-dialectique-negative-theodor-wiesengrund-adorno/

Theodor Adorno, Dialectique négative (1e éd. : 1966), Payot, 1992, p. 286.

.. _7_corcuff_2024_04_05:

(7) Michel Onfray et Éric Naulleau, La gauche réfractaire, Bouquins, 2022
--------------------------------------------------------------------------------


Michel Onfray et Éric Naulleau, La gauche réfractaire, Bouquins, 2022.

.. _8_corcuff_2024_04_05:

(8) Les mots qui fâchent. Contre le maccarthysme intellectuel, sous la direction d’Alain Policar, Nonna Mayer et Philippe Corcuff, éditions de l’aube, 2024 (1e éd. : 2022)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.placedeslibraires.fr/listeliv.php?base=allbooks&mots_recherche=Les+mots+qui+f%C3%A2chent.+Contre+le+maccarthysme+intellectuel
- https://www.librairielesquare.com/livre/9782815956864-les-mots-qui-fachent-contre-le-maccarthysme-intellectuel-philippe-corcuff-alain-policar/

Pour une réponse à ces fantasmes d’extrême droite, de droite,
"macroniste " et de gauche dite "républicaine, voir la réédition en
format de poche de **Les mots qui fâchent. Contre le maccarthysme intellectuel,
sous la direction d’Alain Policar, Nonna Mayer et Philippe Corcuff, éditions
de l’aube**, 2024 (1e éd. : 2022).

.. _9_corcuff_2024_04_05:

(9) Max Weber, "La profession et la vocation de politique " (conférence de 1919)
------------------------------------------------------------------------------------


Max Weber, "La profession et la vocation de politique " (conférence
de 1919), dans Le savant et le politique, préface et traduction de Catherine
Colliot-Thélène, La Découverte/poche, 2003, pp. 185-204.

.. _10_corcuff_2024_04_05:

(10) Clément Rosset, Le réel. Traité de l’idiotie, Minuit, 1977, p. 60.
---------------------------------------------------------------------------


Clément Rosset, Le réel. Traité de l’idiotie, Minuit, 1977, p. 60.

.. _11_corcuff_2024_04_05:

(11) Voir Philippe Corcuff, "De Simenon et Audiard à Médine : d’un humanisme défaillant face à l’antisémitisme ", site du Nouvel Obs, 22 août 2023
------------------------------------------------------------------------------------------------------------------------------------------------------


Voir Philippe Corcuff, "De Simenon et Audiard à Médine : d’un
humanisme défaillant face à l’antisémitisme ", site du Nouvel Obs,
22 août 2023.

.. _12_corcuff_2024_04_05:

(12) Je remercie Grégory Molle qui a découvert la provenance de ce dessin qui circulait sur X (ex-Twitter) en mars 2024 sans références
-----------------------------------------------------------------------------------------------------------------------------------------


Je remercie Grégory Molle qui a découvert la provenance de ce dessin qui
circulait sur X (ex-Twitter) en mars 2024 sans références, parfois confondu
avec le dessin qui faisait l’objet de la polémique.

.. _13_corcuff_2024_04_05:

(13) Philippe Corcuff, "De Pierre Dac à Gaspard Proust, en passant par Charline Vanhoenacker… Rire de tout, sans limites ?"
---------------------------------------------------------------------------------------------------------------------------------


Philippe Corcuff, "De Pierre Dac à Gaspard Proust, en passant par
Charline Vanhoenacker… Rire de tout, sans limites ?", site du Nouvel Obs,
15 juin 2023.

.. _14_corcuff_2024_04_05:

(14) Voir Jean Baubérot, "Faire un pas de côté avec Michel Wieviorka ", blog Mediapart, 21 novembre 2023
------------------------------------------------------------------------------------------------------------


Voir Jean Baubérot, "Faire un pas de côté avec Michel Wieviorka ",
blog Mediapart, 21 novembre 2023.
